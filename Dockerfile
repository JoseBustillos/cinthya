FROM tensorflow/tensorflow:2.0.0-gpu-py3

ENV APP_ROOT /src
ENV CONFIG_ROOT /config

ARG DEBIAN_FRONTEND=noninteractive

#COPY prerequisites.txt .
RUN mkdir ${CONFIG_ROOT}
COPY requirements.txt ${CONFIG_ROOT}/requirements.txt
RUN pip3 install -r ${CONFIG_ROOT}/requirements.txt

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}

ADD . ${APP_ROOT}

EXPOSE 8000